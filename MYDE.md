# My Desktop Environment

## Software installation 

```
vim
vifm
git
qutebrowser
herbstluftwm
```

## Config files


Clone the dotfiles:
```
git clone --separate-git-dir=$HOME/.dotfiles https://framagit.org/Blaireau/dotfiles.git tempdotfiles
rsync -r -v --exclude '.git' tempdotfiles $HOME 
rm -r tempdotfiles
```

Configuration:
```
config config status.showUntrackedFiles nu
```
