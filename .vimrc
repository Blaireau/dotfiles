" REMAP BÉPO
" PLUGINS (mark p)
"
"
" ========================= REMAP BÉPO ==========================
" left-right motions 
noremap t h
noremap r l

noremap T h
noremap R l

   "N lines up/down (default 1/2 window)
noremap S 20j
noremap D 20k
   "to first non-blank in line
noremap c ^
   "to end of line
noremap n $
   "to column N (default 1)
noremap C <bar>
   "till the Nth occurrence of {char}
noremap h t
noremap q T
   "to the Nth occurrence of {char}
noremap H f
noremap Q F

"---------------------------------------------------------------
" up-down motion
noremap d k
noremap s j

" goto line N
noremap B G
noremap bb gg

"---------------------------------------------------------------
"text object motions

"words
noremap l w
noremap L W
noremap j e
noremap J E
noremap v b
noremap V B


"---------------------------------------------------------------
"pattern searches

" recherche
noremap f /
noremap m n
noremap M N


" mot suivant sous le curseur
noremap z *
noremap Z #

"---------------------------------------------------------------
"Marks and motions

"set mark
noremap $ m
" jump to mark
noremap g '
noremap gg ''

"---------------------------------------------------------------
"inserting text

noremap e i
vnoremap E I
noremap E I
noremap , a
noremap ; A
vnoremap ; A
noremap x o
noremap X O

"---------------------------------------------------------------
"deleting text

" suppression
noremap a d
noremap A D
" join lines
noremap à J

"---------------------------------------------------------------
"copying and pasting text

noremap u y
noremap U Y
noremap i p
noremap I P
"register key
noremap ê "


"---------------------------------------------------------------
"changing text

noremap y c
noremap Y C
"set case
vnoremap k u
vnoremap K U
"format wrap
vnoremap <return> gq

noremap O <c-a>
noremap P <c-x>
nnoremap <return> o<esc>k 

" indentation
noremap o >
noremap p <

"---------------------------------------------------------------
"visual mode
noremap <nowait> è v
noremap È V
noremap èè <c-v>

"---------------------------------------------------------------
"escape
inoremap þ <Esc>
inoremap Þ <Esc>
vnoremap þ <Esc>
vnoremap Þ <Esc>
cnoremap þ <C-C>
cnoremap Þ <C-C>

"---------------------------------------------------------------
"macros

"record macro
noremap w q
"execute register
noremap ç @

"---------------------------------------------------------------
"enter command mode
noremap . :

"undo
noremap é u
noremap É <C-R>

"---------------------------------------------------------------
"commenting stuff
xmap ' <Plug>Commentary
nmap ' <Plug>Commentary
omap ' <Plug>Commentary
nmap '' <Plug>CommentaryLine


"---------------------------------------------------------------
"buffers
noremap <c-l> :bnext<cr>
noremap <c-v> :bprevious<cr>

"split and windows
noremap <c-t> <c-w>h 
noremap <c-r> <c-w>l
noremap <c-d> <c-w>k
noremap <c-s> <c-w>j

"---------------------------------------------------------------
" jump to definition
noremap ^ <C-]>

" show documentation
noremap - K

" leader key
let mapleader = ' '
nnoremap <Leader>a :echo "test"<cr>

" completion
" inoremap <c-t> <c-p>
" inoremap <c-r> <c-n>

"---------------------------------------------------------------
"show code structure
"'7' will toggle the tagbar
nmap + :TagbarOpenAutoClose<cr>
let g:tagbar_map_help = ''
let g:tagbar_map_jump = ['e', '<Enter>']
let g:tagbar_map_preview = ''
let g:tagbar_map_previewwin = ''
let g:tagbar_map_nexttag = 'S'
let g:tagbar_map_prevtag = 'D'
let g:tagbar_map_showproto = ''
let g:tagbar_map_hidenonpublic = ''
let g:tagbar_map_openfold = 'r'
let g:tagbar_map_closefold = 't'
let g:tagbar_map_togglefold = ''
let g:tagbar_map_openallfolds = 'l'
let g:tagbar_map_closeallfolds = 'v'
let g:tagbar_map_incrementfolds = ''
let g:tagbar_map_decrementfolds = ''
let g:tagbar_map_nextfold = ''
let g:tagbar_map_prevfold = ''
let g:tagbar_map_togglesort = ''
let g:tagbar_map_toggleautoclose = ''
let g:tagbar_map_togglepause = ''
let g:tagbar_map_zoomwin = ''
let g:tagbar_map_close = ['q', 'þ', 'Þ']


"---------------------------------------------------------------
"spell checking

function! ToggleSpell()
  if !exists("g:showingSpell")
    let g:showingSpell=0
    setlocal spell
  endif

  if g:showingSpell==0
    execute "hi SpellBad cterm=underline ctermfg=red"
    let g:showingSpell=1
    noremap z ]s
    noremap Z [s
    noremap = zg
    noremap % z= 
  else
    execute "hi clear SpellBad"
    let g:showingSpell=0
    noremap z * 
    noremap Z # 
  endif
endfunction

function! SetSpellLang()
    call inputsave()
    let lang = input('Language: ')
    call inputrestore()
    setlocal spell
    let &l:spelllang = lang
    if !exists("g:showingSpell")
      let g:showingSpell=0
      execute "hi clear SpellBad"
    endif
endfunction

nmap <silent> * :call ToggleSpell()<CR>
nmap <silent> 0 :call SetSpellLang()<CR> 

"---------------------------------------------------------------
"running and compiling
let b:run_command = ''

function RunCurrentFile()
    " store the command in a local variable before creating the terminal
    " buffer, otherwise the buffer variable is not accessible anymore
    let l:cmd = b:run_command."\<CR>"
    const term_buf = term_start(&shell, {'term_finish':'close'})
    call term_sendkeys(term_buf, l:cmd)
endfunction

function SetRunCmd(cmd)
    let b:run_command = a:cmd
endfunction

nmap <silent> " :w<CR>:call RunCurrentFile()<CR>

"toogle bool
function BoolToggle()
    let translate = {
                \  "True": "False",
                \  "False": "True",
                \  "true": "false",
                \  "false": "true"
                \}
    let findRegex = "True\\|true\\|False\\|false"
    let currentLine = getline('.')
    echo col('.')
    let [word, mstart, mend] = matchstrpos(currentLine, findRegex, col('.')-1)
    if word == ""
        return
    endif
    let replaceWord = translate[word]
    let newLine = currentLine[:mstart-1].replaceWord.currentLine[mend:]
    call setline(line('.'), newLine) 
endfunction

map <silent> @ :call BoolToggle()<cr>

autocmd FileType asciidoc call SetRunCmd("asciidoctor-pdf ".expand("%"))
autocmd FileType c call SetRunCmd("gcc -o ".expand("%:r")." ".expand("%")." && ./".expand("%:r"))
autocmd FileType dot call SetRunCmd("dot ".expand("%")." -Tpng -o ".expand("%:r").".png")
autocmd FileType haskell call SetRunCmd("ghc -dynamic -outputdir bin ".expand("%")." && ./".expand("%:r"))
autocmd FileType markdown call SetRunCmd("pandoc ".expand("%")." -o ".expand("%:r").".pdf")
autocmd FileType matlab call SetRunCmd("octave ".expand("%"))
autocmd FileType python call SetRunCmd("python3 ".expand("%"))
autocmd FileType scala call SetRunCmd("scala ".expand("%"))
autocmd FileType sh call SetRunCmd("bash ".expand("%"))
autocmd FileType tex call SetRunCmd("pdflatex ".expand("%"))
autocmd FileType zinc call SetRunCmd("minizinc ".expand("%"))

autocmd BufNewFile,BufRead *.slides set filetype=markdown | call SetRunCmd("mdslides --include media ".expand("%")) | setlocal commentstring=<!--%s-->

autocmd FileType kivy setlocal commentstring=#\ %s
autocmd FileType dot setlocal commentstring=#\ %s
autocmd FileType vhdl  setlocal commentstring=--\ %s
autocmd FileType vifm setlocal commentstring=\"\ %s
autocmd FileType zinc  setlocal commentstring=\%\ %s
autocmd FileType cabal  setlocal commentstring=--\ %s

autocmd FileType kivy  setlocal sw=4

autocmd FileType vifm setlocal syntax=vim 

autocmd FileType tex\|markdown set wrap | set linebreak

autocmd BufNewFile,BufRead *.py set keywordprg=pydoc

autocmd FileType html,php,markdown,text,tex,asciidoc,mail,gitcommit
    \ runtime macros/emoji-ab.vim

" =========================== PLUGINS (vim-plug) ===========================
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
" Plug 'preservim/nerdtree'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'ap/vim-css-color'
Plug 'preservim/tagbar'
Plug 'universal-ctags/ctags'
Plug 'sirver/ultisnips'
    let g:UltiSnipsExpandTrigger = '<tab>'
    let g:UltiSnipsJumpForwardTrigger = '<tab>'
    let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
Plug 'lervag/vimtex'
    let g:tex_flavor='latex'
    let g:vimtex_syntax_enabled=0
    let g:vimtex_comiler_enabled=0
    let g:vimtex_complete_enabled=0
    let g:vimtex_fold_enabled=0
    let g:vimtex_imaps_enabled=0
    let g:vimtex_mappings_enabled=0
Plug 'beyondmarc/glsl.vim'
" Plug 'liuchengxu/graphviz.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'goerz/jupytext.vim'
Plug 'dhruvasagar/vim-table-mode'
Plug 'https://gitlab.com/gi1242/vim-emoji-ab'
Plug 'udalov/kotlin-vim'


" Initialize plugin system
call plug#end()

" =========================== CONFIGURATION ===========================
syntax on

set nocompatible

set path+=**
set wildmenu
set clipboard=unnamedplus "default copy to system clipboard

set noerrorbells "pas de sons en cas d erreur
set number
set ruler

set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set expandtab
set smartindent
set autoread

set incsearch
set enc=utf-8
set scrolloff=7
set colorcolumn=80
set magic

" colorise les nbsp
highlight NbSp ctermbg=lightgray guibg=lightred
match NbSp /\%xa0/
set listchars=nbsp:␣
set list

" let g:table_mode_map_prefix = '_'
let g:table_mode_toggle_map = 't'
let g:table_mode_motion_up_map = '<c-d>'
let g:table_mode_motion_down_map = '<c-s>'
let g:table_mode_motion_left_map = '<c-t>'
let g:table_mode_motion_right_map = '<c-r>'
let g:table_mode_delete_row_map = '<Leader>tar'
let g:table_mode_delete_column_map = '<Leader>tac'
let g:table_mode_insert_column_before_map = '<Leader>te'
let g:table_mode_insert_column_after_map = '<Leader>t,'
let g:table_mode_tableize_map = '<Leader>tm'
let g:table_mode_verbose = 1
let g:table_mode_auto_align = 1

let g:gutentags_exclude_filetypes=[".*.json"] 

" connaitre le highlight group
map <F7> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" ========================= SYNTAXE PYTHON ============================
let g:python_highlight_all = 1
let python_highlight_all = 1

set termguicolors
colorscheme monokai_revise
":colorscheme bl_theme
